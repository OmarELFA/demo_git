'''sample code of addition'''


def add2(a: int, b: int) -> int:
    "sum of two number"
    return a + b

if __name__=='__main__':
    print(add2(2, 3))
